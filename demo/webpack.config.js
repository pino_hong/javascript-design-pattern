const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [{
      test: /.js$/,
      exclude: /node_modules/,
      use: ['babel-loader']
    },{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/,
    },]
  },
  devServer: {
    port: 3002,
    open: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html'
    })
  ]
}