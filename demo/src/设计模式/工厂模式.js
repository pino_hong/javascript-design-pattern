// 
/**
 * 将new 操作单独封装
 * 遇到new时，就要考虑是否该使用工厂模式
 * ------------------------------------
 * 工厂模式的主要目的是用来创建对象的一种常见的设计模式。
 */

/**需求
 * 去购买汉堡，直接点餐，取餐，不会自己亲手做
 * 商店要“封装”做汉堡的工作，做好直接给买者。
 */

class Product {
  constructor(name){
    this.name = name
  }
  init(){
    console.log('init')
  }
  fn1(){
    console.log('fn1')
  }
  fn2(){
    console.log('fn2')
  }
}
// 工厂
class Creator {
  create(name){
    this.name = new Product(this.name)
  }
}

const creator = new Creator()
const xiezi = creator.create('xiezi')
xiezi.init()



