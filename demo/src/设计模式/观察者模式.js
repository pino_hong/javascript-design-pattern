
/*
class Subject {
  constructor() {
    this.state = 0
    this.observers = []
  }
  getState() {
    return this.state
  }
  setState(state) {
    this.state = state
    this.notifyAllObservers()
  }
  notifyAllObservers() {
    this.observers.forEach((observer) => {
      observer.update()
    })
  }
  attach(observer) {
    this.observers.push(observer)
  }
}

class Observer {
  constructor(name, subject) {
    this.name = name
    this.subject = subject
    this.subject.attach(this)
  }
  update() {
    console.log(`${this.name} update ,state ${this.subject.getState()}`)
  }
}

const s = new Subject()
const o1 = new Observer('o1', s)

const o2 = new Observer('o2', s)
// console.log(o1)
s.setState(1)
/*

/*
class PubSub {
  constructor() {
    this.lientList = {} // 缓存列表
  }
  // 增加订阅者
  listen(key, fn) {
    if (!this.lientList[key]) {
      this.lientList[key] = []
    }
    this.lientList[key].push(fn)
  }
  // 发布消息
  trigger(...val) {
    const key = val.shift()
    const fns = this.lientList[key]
    if (!fns || fns.length === 0) {
      return false
    }
    fns.forEach((fn) => {
      fn.apply(this, [key, ...val])
    })
  }
  // 取消订阅
  remove(key, fn) {
    const fns = this.lientList[key]
    if (!fns) {
      return
    }
    if (!fn) {
      fns.length = 0
    } else {
      fns.forEach((_fn, i) => {
        if (_fn === fn) {
          fns.splice(i, 1)
        }
      })
    }
  }
}

function subscriber(name, price, squareMeter) {
  console.log(`用户：${name},平米：${squareMeter},价格：${price}`)
}
const salesOffices = new PubSub()
// 订阅消息
salesOffices.listen('xiaoming', subscriber)
salesOffices.listen('lanlan', subscriber)
salesOffices.listen('lanlan', subscriber)

salesOffices.remove('lanlan', subscriber) // 删除蓝蓝指定的订阅消息
salesOffices.trigger('lanlan', 66, 888888) //向蓝蓝发布消息
*/

class WechatServer{
  constructor(){
      this.list = [];
  }
  registerObserver(o) {
      this.list.push(o);
  };
  removeObserver(o){
      for (var i = 0; i < this.list.length; i++) {
          if (this.list[i] === o) {
              this.list.splice(i, 1);
              break;
          }
      }
  }
  notifyObserver() {
      for (var i = 0; i < this.list.length; i++) {
          var oserver = this.list[i];
          oserver.update(this.message);
      }
  }
  setInfomation(s){
      this.message = s;
      console.log(`微信服务更新消息：${s}`);
      //消息更新，通知所有观察者
      this.notifyObserver();
  }
}

/**
* 观察者
*/
class User{
  constructor(name) {
      this.name = name;
  }
  update(message) {
      this.message = message;
      this.read();
  };
  read (){
      console.log(`${this.name}收到推送消息：${this.message}`);
  };
};

/**
* 我们来测试
*/
let server = new WechatServer();
let userZhang = new User("张三");
let userLi = new User("李四");
let userWang = new User("王五");
server.registerObserver(userZhang);
// server.registerObserver(userLi);
// server.registerObserver(userWang);
server.setInfomation("PHP是世界上最好用的语言！");
// console.log("-----用户张三看到消息后颇为震惊，果断取消订阅，这时公众号又推送了一条消息，此时用户ZhangSan已经收不到消息，其他用户还是正常能收到推送消息----------------");
// server.removeObserver(userZhang);
// server.setInfomation("JAVA是世界上最好用的语言！");


class Subject {
  constructor(){
    this.observers = {}
    this.key = ''
  }
  add(observer){
    const key = observer.project
    if (!this.observers[key]) {
      this.observers[key] = []
    }
    this.observers[key].push(observer)
  }
  remove(observer){
    const _observers = this.observers[observer.project]
    console.log(_observers,11)
    if(_observers.length){
      _observers.forEach((item,index)=>{
        if(item === observer){
          _observers.splice(index,1)
        }
      })
    }
  }
  setObserver(subject){
    this.key = subject
    this.notifyAllObservers()
  }
  notifyAllObservers(){
    this.observers[this.key].forEach((item,index)=>{
      item.update()
    })
  }
}

class Observer {
  constructor(project,name) {
    this.project = project
    this.name = name
  }
  update() {
    console.log(`尊敬的:${this.name} 你预约的项目：【${this.project}】 马上开始了`)
  }
}

const subject = new Subject()
const xiaoming = new Observer('滑雪','xiaoming')
const A = new Observer('大跳台','A')
const B = new Observer('大跳台','B')
const C = new Observer('大跳台','C')
subject.add(xiaoming)
subject.add(A)
subject.add(B)
subject.add(C)
subject.remove(B)
subject.setObserver('大跳台')
/** 执行结果
 * 尊敬的:A 你预约的项目：【大跳台】 马上开始了
 * 观察者模式.js:215 尊敬的:C 你预约的项目：【大跳台】 马上开始了
 */