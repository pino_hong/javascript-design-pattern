/**
 * 系统中被唯一使用
 * 一个类只有一个实例
 * --------------------
 * 单例模式需要用到java的特性privete
 * 
 * 单例模式的思路是：保证一个类只能被实例一次，每次获取它的时候，如果没有则创建一个实例保存并返回，如果已经存在则直接返回已存在的实例。
 */

class SingleObject {
  login(){
    console.log('login...')
  }
  static instance = null
  static getInstance(){
    if(!SingleObject.instance){
      SingleObject.instance = new SingleObject()
    }
    return SingleObject.instance
  }
}
console.log(SingleObject.getInstance() === SingleObject.getInstance())
SingleObject.getInstance().login()


/** 例子1 登录框
 * 
 */
class LoginFrame {
  constructor(state){
    this.state = 'hide'
  }
  show(){
    if(this.state === 'show'){
      console.log('登录框已显示')
      return
    }
    this.state = 'show'
    console.log('登录框显示成功')
  }
  hide(){
    if(this.state === 'hide'){
      console.log('登录框已隐藏')
      return
    }
    this.state = 'hide'
    console.log('登录框隐藏成功')
  }
  static getInstance(start){
    console.log(this)
    if(!this.instance){
      this.instance = new LoginFrame(start)
    }
    return this.instance
  }
}
// LoginFrame.getInstance = (function(){
//   let instance
//   return function(start){
//     if(!instance){
//       return instance = new LoginFrame(start)
//     }
//     return instance
//   }
// })()
const p1 = LoginFrame.getInstance('show')
const p2 = LoginFrame.getInstance('hide')
p1.hide()
p2.hide()
console.log(p1 === p2)
console.log(111111)
/**
 * 设计原则验证
 *  符合单一职责原则，只实例化唯一的对象
 *  没法具体开放封闭原则，但是绝对不违反开放封闭原则
 */