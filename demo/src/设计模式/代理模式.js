/**
 * 使用者无权访问对象
 * 中间加代理，通过代理做授权和控制
 */

class ReadImg {
  constructor(fileName) {
    this.fileName = fileName
    this.loadFromDisk()
  }
  display() {
    console.log('display...' + this.fileName)
  }
  loadFromDisk() {
    console.log('loading...' + this.fileName)
  }
}

class ProxyImg {
  constructor(fileName) {
    this.realImg = new ReadImg(fileName)
  }
  display() {
    this.realImg.display()
  }
  loadFromDisk() {
    this.realImg.loadFromDisk()
  }
}

const proxyImg = new ProxyImg('index.html')



class Staff {
  constructor(affairType) {
    this.affairType = affairType
  }
  applyFor(target) {
    target.receiveApplyFor(this.affairType)
  }
}
class Secretary {
  constructor() {
    this.leader = new Leader()
  }
  receiveApplyFor(affair) {
    this.leader.receiveApplyFor(affair)
  }
}
class Leader {
  receiveApplyFor(affair) {
    console.log(`批准:${affair}`)
  }
}

const staff = new Staff('升职加薪')
staff.applyFor(new Secretary()) // 批准:升职加薪
