/*
class GoogleAddHandler {
  addEventListener(type, handler){
    return this.element.addEventListener(type, handler, false);
  }
}
class IEAddHandler {
  attachEvent(type, handler){
    return element.attachEvent("on" + type, handler);
  }
}

class AdaptiveAddHandler {
  constructor(element){
    this.element = element
  }
  addEventListener(type, handler){
    const iEAddHandler = new IEAddHandler()
    iEAddHandler.attachEvent(type, handler)
  }
}
*/
class Jack {
  english() {
    return 'I speak English'
  }
}
class Xiaoming {
  chinese() {
    return '我只会中文'
  }
}
// 适配器
class XiaoHong {
  constructor(person) {
    this.person = person
  }
  chinese() {
    return `${this.person.english()} 翻译： "我会说英语"`
  }
}
class Communication {
  speak(language) {
    console.log(language.chinese())
  }
}

const xiaoming = new Xiaoming()
const xiaoHong = new XiaoHong(new Jack())
const communication = new Communication()
communication.speak(xiaoming)
communication.speak(xiaoHong)