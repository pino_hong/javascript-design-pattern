/**
 * 为对象添加新功能
 * 不改变其原有结构和功能
 * 通俗的说，为原有的东西添加装饰，如给手机带一个手机壳
 */
/*
class Circle {
  draw() {
    console.log('画一个圆形')
  }
}
// 装饰器
class Decorator {
  constructor(circle) {
    this.circle = circle
  }
  draw() {
    this.circle.draw()
    this.setRedBorder(this.circle)
  }
  setRedBorder(circle) {
    console.log('设置红色边框')
  }
}

// 测试代码
let circle = new Circle()
circle.draw()
console.log('-----------------')
let dec = new Decorator(circle)
dec.draw()
*/

/**
 * ES7装饰器
 * transform-decorators-legacy
 */
/*
@testDec
class Demo {

}

function testDec(target) {
  target.isDec = true
}
console.log(Demo.isDec)
*/

function mixins(...list){
  console.log(...list,1)
  return function(target){
    console.log(target,2)
    Object.assign(target.prototype,...list)
  }
}

const Foo = {
  foo(){
    console.log('foo')
  }
}
@mixins(Foo)
class MyClass {

}

let obj = new MyClass()
obj.foo()
//-----------------------------------
/*
function readonly(target,name,descriptor){
  console.log(target)
  console.log(name)
  console.log(descriptor)
  console.log('========')
  descriptor.writable = false
  return descriptor
}

class Person {
  constructor(){
    this.first = 'A'
    this.last = 'B'
  }
  @readonly
  name(){
    return `${this.first} ${this.last}`
  }
}

const p = new Person()
console.log(p.name())
p.first = function(){
  console.log(100)
}
*/

function log(target,name,descriptor){
  let oldVal = descriptor.value
  descriptor.value = function(){
    console.log(`calling ${name} width`,arguments)
    return oldVal.apply(this,arguments)
  }
  return descriptor
}

class Math {
  @log
  add(a,b){
    return a+b
  }
}

const math = new Math()
const result = math.add(2,4)
console.log('resilt',result)

class Aircraft {
  ordinary(){
      console.log('发射普通子弹')
  }
}
class AircraftDecorator {
  constructor(aircraft){
      this.aircraft = aircraft
  }
  laser(){
      console.log('发射激光')
  }
  guidedMissile(){
      console.log('发射导弹')
  }
  ordinary(){
      this.aircraft.ordinary()
  }
}
const aircraft = new Aircraft()
const aircraftDecorator = new AircraftDecorator(aircraft)
console.log(aircraftDecorator.ordinary()) // 发射普通子弹
console.log(aircraftDecorator.laser()) // 发射激光
console.log(aircraftDecorator.guidedMissile()) // 发射导弹