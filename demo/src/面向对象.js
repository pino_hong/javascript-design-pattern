// let a = 1
// console.log(a, '-------------')
// console.log(Promise.resolve(1), '-----')
/**
 * 什么是面向对象？
 *  哪些概念
 *    类和对象
 *  面向对象三要素：
 *    继承：子类继承父类
 *      继承可将公共方法抽离出来，提高复用，减少冗余
 *    封装：数据的权限和保密
 *      public 完全开放
 *      protected 只对子类开放
 *      private 只对自己开放
 *      减少耦合，不该外露的不外露
 *      利于数据，接口的权限管理
 *    多态：同一接口不同的实现
 *      保持子类的开放性和灵活性
 *      面向接口编程
 *  js的应用举例
 *  面向对象的意义
 */

 class JQuery {
  constructor(seletor){
    const slice = Array.prototype.slice
    const dom = slice.call(document.querySelectorAll(seletor))
    const len = dom?dom.length:0
    for (let i = 0; i < len; i++) {
      this[i] = dom[i]
    }
    this.length = len
    this.seletor = seletor || ''
  }
  append(node){

  }
  addClass(name){

  }
  html(data){

  }
}

window.$ = function(seletor){
  // 工厂模式
  return new JQuery(seletor)
}

console.log($('div')[1])

// 为何使用面向对象
/**
 * 程序执行：顺序，判断，循环---结构化
 * 面向对象 --- 数据结构化
 * 对于计算机，结构化的才是最简单
 * 编程应该 简单&抽象
 */



