import './设计模式/观察者模式'
// import './设计模式/外观模式'
// import './设计模式/代理模式'
// import './设计模式/装饰器模式'
// import './设计模式/适配器模式'
// import './设计模式/单例模式'
// import './设计模式/工厂模式'

// import './面向对象'
/*
function loadImg(src) {
  let promise = new Promise((resolve, reject) => {
    let img = document.createElement('img')
    img.onload = function () {
      resolve(img)
    }
    img.onerror = function (err) {
      reject('图片加载失败')
    }
    img.src = src
  })
  return promise
}
const src = 'https://img2.baidu.com/it/u=690487400,3736912659&fm=253&fmt=auto&app=120&f=JPEG?w=1200&h=675'
const result = loadImg(src)
result.then(res => {
  console.log(res.width, res.height)
}).catch(err => {
  console.log(err)
})

class Car {
  constructor(number, name) {
    this.number = number
    this.name = name
  }
}
class Kuaiche extends Car {
  constructor(number, name) {
    super(number, name)
    this.price = 1
  }
}
class Zhuanche extends Car {
  constructor(number, name) {
    super(number, name)
    this.price = 2
  }
}
class Trip {
  constructor(car) {
    this.car = car
  }
  start() {
    console.log(`你当前乘坐的车是：${this.car.name}，车牌号：${this.car.number}`)
  }
  end() {
    console.log(`车辆已到站，请支付${this.car.price * 5}元`)
  }
}
const kuaiche = new Kuaiche('赣G67D89', '宝来')
const trip = new Trip(kuaiche)
trip.start()
trip.end()
*/