/**
 * 中介者模式的作用就是解除对象与对象之间的紧耦合关系。增加一个中介者对象后，所有的相关对象都通过中介者对象来通信，而不是互相引用，所以当一个对象发生改变时，只需要通知中介者对象即可。中介者使各对象之间耦合松散，而且可以独立地改变它们之间的交互。中介者模式使网状的多对多关系变成了相对简单的一对多关系。
 */
/*
function Player(name) {
  this.name = name
  this.enemy = null
}
Player.prototype.win = function () {
  console.log(this.name + 'won')
}
Player.prototype.lose = function () {
  console.log(this.name + 'lost')
}
Player.prototype.die = function () {
  this.lose()
  this.enemy.win()
}

// const player1 = new Player('xiaoming')
// const player2 = new Player('xiaohong')

// player1.enemy = player2
// player2.enemy = player1



var players = [];

function Player(name, teamColor) {
  this.partners = []; // 队友列表
  this.enemies = []; // 敌人列表
  this.state = 'live'; // 玩家状态
  this.name = name; // 角色名字
  this.teamColor = teamColor; // 队伍颜色
};

Player.prototype.win = function () { // 玩家团队胜利
  console.log('winner: ' + this.name);
};
Player.prototype.lose = function () { // 玩家团队失败
  console.log('loser: ' + this.name);
};

Player.prototype.die = function () { // 玩家死亡
  var all_dead = true;
  this.state = 'dead'; // 设置玩家状态为死亡
  for (var i = 0, partner; partner = this.partners[i++];) { // 遍历队友列表
    if (partner.state !== 'dead') { // 如果还有一个队友没有死亡，则游戏还未失败
      all_dead = false;
      break;
    }
  }
  if (all_dead === true) { // 如果队友全部死亡
    this.lose(); // 通知自己游戏失败
    for (var i = 0, partner; partner = this.partners[i++];) { // 通知所有队友玩家游戏失败
      partner.lose();
    }
    for (var i = 0, enemy; enemy = this.enemies[i++];) { // 通知所有敌人游戏胜利
      enemy.win();
    }
  }
};

var playerFactory = function (name, teamColor) {
  var newPlayer = new Player(name, teamColor); // 创建新玩家
  for (var i = 0, player; player = players[i++];) { // 通知所有的玩家，有新角色加入
    if (player.teamColor === newPlayer.teamColor) { // 如果是同一队的玩家
      player.partners.push(newPlayer); // 相互添加到队友列表
      newPlayer.partners.push(player);
    } else {
      player.enemies.push(newPlayer); // 相互添加到敌人列表
      newPlayer.enemies.push(player);
    }
  }
  players.push(newPlayer);
  return newPlayer;
};

var player1 = playerFactory('皮蛋', 'red'),
  player2 = playerFactory('小乖', 'red'),
  player3 = playerFactory('宝宝', 'red'),
  player4 = playerFactory('小强', 'red');

var player5 = playerFactory('黑妞', 'blue'),
  player6 = playerFactory('葱头', 'blue'),
  player7 = playerFactory('胖墩', 'blue'),
  player8 = playerFactory('海盗', 'blue');

player1.partners = [player1, player2, player3, player4];
player1.enemies = [player5, player6, player7, player8];
player5.partners = [player5, player6, player7, player8];
player5.enemies = [player1, player2, player3, player4];

player1.die();
player2.die();
player4.die();
player3.die();
// player1.die()
// console.log(player1)
// console.log(player2) 
*/

// 中介者模式改造泡泡堂

function Player(name, teamColor) {
  this.name = name // 角色名字
  this.teamColor = teamColor // 队伍颜色
  this.state = 'alive' // 玩家生存状态
}
Player.prototype.win = function () {
  console.log(this.name + ' won')
}
Player.prototype.lose = function () {
  console.log(this.name + ' lost')
}
// -----------------玩家死亡---------------
Player.prototype.die = function () {
  this.state = 'dead'
  playerDirector.reciveMessage('playerDead', this) //给中介者发送消息，玩家死亡
}
// ----------------移除玩家-----------------------
Player.prototype.remove = function () {
  playerDirector.reciveMessage('removePlayer', this)
}
// ----------------玩家换队----------------------
Player.prototype.changeTeam = function (color) {
  playerDirector.reciveMessage('changeTeam', this, color) // 给中介者发送消息，玩家换队
}

// 工厂函数
function playerFactory(name, teamColor) {
  var newPlayer = new Player(name, teamColor); // 创造一个新的玩家对象
  playerDirector.reciveMessage('addPlayer', newPlayer); // 给中介者发送消息，新增玩家
  return newPlayer;
}

/**创建中介者对象
 *  实现该功能的两种方案。
 *    利用发布-订阅模式，将playerDirector实现为订阅者，各player作为发布者，一旦player状态发生改变，便推送消息给playerDirector,playerDirector处理消息后将反馈发送给其他player。
 *    在playerDirector中开放一些接受消息的接口，各player可以直接调用该接口来给playerDirector发送消息，player只需传递一个参数给playerDirector，这个参数的目的是使playerDirector可以识别发送者，同时，playerDirector接受到消息之后会将处理结果反馈给其他的player
 */

const playerDirector = (function () {
  const players = {} // 保存所有玩家
  const operations = {} // 中介者可以执行的操作
  // ----------------新增一个玩家----------------------
  operations.addPlayer = function (player) {
    const teamColor = player.teamColor // 玩家队伍的颜色
    players[teamColor] = players[teamColor] || []// 如果该颜色的玩家还没有成立队伍，则新成立一个队伍。
    players[teamColor].push(player) // 添加玩家进队伍
    // -----------------移除一个玩家-------------------------
  }
  operations.removePlayer = function (player) {
    const teamColor = player.teamColor // 玩家队伍的颜色
    const teamPlayers = players[teamColor] || [] // 该队伍的所有成员
    for (let i = teamPlayers.length - 1; i >= 0; i--) {//遍历删除
      if (teamPlayers[i] === player) {
        teamPlayers.splice(i, 1)
      }
    }
  }
  // --------------------玩家换队------------------
  operations.changeTeam = function (player, newTeamColor) {
    operations.removePlayer(player) // 从原队伍中删除
    player.teamColor = newTeamColor // 改变队伍的颜色
    operations.addPlayer(player) // 增加到新队伍中
  }
  operations.playerDead = function (player) { // 玩家死亡
    const teamColor = player.teamColor
    let teamPlayers = players[teamColor] // 玩家所在队伍
    let all_dead = true
    for (let i = 0, player; player = teamPlayers[i++];) {
      if (player.state !== 'dead') {
        all_dead = false
        break
      }
    }
    if (all_dead === true) { // 全部死亡
      for (var i = 0, player; player = teamPlayers[i++];) {
        player.lose(); // 本队所有玩家 lose
      }
      for (var color in players) {
        // console.log(color)
        if (color !== teamColor) {
          teamPlayers = players[color]; // 其他队伍的玩家
          for (var i = 0, player; player = teamPlayers[i++];) {
            player.win(); // 其他队伍所有玩家 win
          }
        }
      }
    }
  }
  var reciveMessage = function () {
    var message = Array.prototype.shift.call(arguments); // arguments 的第一个参数为消息名称
    operations[message].apply(this, arguments);
  };
  return {
    reciveMessage: reciveMessage
  }
})()

var player1 = playerFactory('皮蛋', 'red'),
  player2 = playerFactory('小乖', 'red'),
  player3 = playerFactory('宝宝', 'red'),
  player4 = playerFactory('小强', 'red');

var player5 = playerFactory('黑妞', 'blue'),
  player6 = playerFactory('葱头', 'blue'),
  player7 = playerFactory('胖墩', 'blue'),
  player8 = playerFactory('海盗', 'blue');

player1.die();
player2.die();
player3.die();
player4.die();

// 假设皮蛋从红队叛变到蓝队，
// player1.remove();
// player2.remove();
// player3.die();
// player4.die();

// 假设皮蛋从红队叛变到蓝队，
// player1.changeTeam( 'blue' );
// player2.die();
// player3.die();
// player4.die();