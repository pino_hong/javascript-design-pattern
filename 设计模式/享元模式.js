// 享元模式是一种用于性能优化的模式，核心是运用共享技术来有效支持大量的细粒度的对象
// 如果系统中因为创建了大量类似的对象而导致内存占用过高，这个时候享元模式就非常有用了。
/** 内部状态与外部状态
 * 享元模式要求将对象的属性划分为内部状态与外部状态（状态在这里指属性）。关于区分外部状态和内部状态说明
 *  内部状态存储于对象的内部
 *  内部状态可以被一些对象共享
 *  内部状态独立于具体的场景，通常不会改变
 *  外部状态取决于具体的场景，并根据场景而变化，外部状态不能被共享。
 * 使用享元模式的关键是如何区分内部状态和外部状态，可以被对象共享的属性通常被划分为内部状态。而外部状态取决于具体的场景，并根据场景而变化。
 */
/*
function Model(sex){
  this.sex = sex
}
Model.prototype.takePhoto = function(){
  console.log(`性别：${this.sex} 内衣：${this.underwear}`)
}

const maleModel = new Model('male')
      femaleModel = new Model('female')

for (let i = 0; i < 50; i++) {
  maleModel.underwear = '男underwear'+i
  maleModel.takePhoto()
}

for (let i = 0; i < 50; i++) {
  femaleModel.underwear = '女underwear'+i
  femaleModel.takePhoto()
}
*/
//文件上传例子
/*
let id = 0
window.startUpload = function (uploadType, files) {
  for (let i = 0, file; file = files[i++];) {
    const uploadObj = new Upload(uploadType, file.fileName, file.fileSize)
    uploadObj.init(id++)
  }
}

function Upload(uploadType, fileName, fileSize) {
  this.uploadType = uploadType
  this.fileName = fileName
  this.fileSize = fileSize
  this.dom = null
}
Upload.prototype.init = function (id) {
  const _this = this
  this.id = id
  this.dom = document.createElement('div')
  this.dom.innerHTML = `<span>文件名称：${this.fileName},文件大小${this.fileSize}</span><button class="delFile">删除</button>`
  this.dom.querySelector('.delFile').onclick = function () {
    _this.delFile()
  }
  console.log(this.dom)
  document.body.append(this.dom)
}

Upload.prototype.delFile = function () {
  if (this.fileSize < 3000) {
    return this.dom.parentNode.removeChild(this.dom)
  }
  if (window.confirm('确定是否要删除该文件吗?' + this.fileName)) {
    return this.dom.parentNode.removeChild(this.dom)
  }
}

startUpload('plugin', [
  {
    fileName: '1.txt',
    fileSize: 1000
  },
  {
    fileName: '2.html',
    fileSize: 3000
  },
  {
    fileName: '3.txt',
    fileSize: 5000
  }
])

startUpload('flash',[
  {
    fileName: '4.txt',
    fileSize: 1000
  },
  {
    fileName: '5.html',
    fileSize: 3000
  },
  {
    fileName: '6.txt',
    fileSize: 5000
  }
])
*/

// 以享元模式方式重构文件上传-------------
let id = 0

// 工厂进行对象实例化
// 定义一个工厂来创建upload对象，如果某种内部状态对应的共享对象已经被创建过，那么直接返回这个对象，否则创建一个新的对象。
const UploadFactory = (function () {
  const createFlyWeightObjs = {}
  return {
    create: function (uploadType) {
      if (createFlyWeightObjs[uploadType]) {
        return createFlyWeightObjs[uploadType]
      }
      return createFlyWeightObjs[uploadType] = new Upload(uploadType)
    }
  }
})()

// 管理器封装外部状态
const uploadManager = (function () {
  const uploadDatabase = {}
  return {
    // 创建属性并且进行缓存，创建dom，并且给dom绑定删除事件。
    add: function (id, uploadType, fileName, fileSize) {
      // 是否存在uploadType为key的属性，有则返回，没有则创建Upload实例对象并且返回
      const flyWeightObj = UploadFactory.create(uploadType)
      const dom = document.createElement('div')
      dom.innerHTML = `<span>文件名称：${fileName}，文件大小：${fileSize}</span><button class="delFile">删除</button>`
      dom.querySelector('.delFile').onclick = function () {
        flyWeightObj.delFile(id)
      }
      document.body.append(dom)
      uploadDatabase[id] = {
        fileName: fileName,
        fileSize: fileSize,
        dom: dom
      }
      return flyWeightObj
    },
    // 绑定外部属性
    setExternalState: function (id, flyWeightObj) {
      const uploadData = uploadDatabase[id]
      for (const key in uploadData) {
        flyWeightObj[key] = uploadData[key]
      }
    }
  }
})()
// 开始加载
window.startUpload = function (uploadType, files) {
  for (let i = 0, file; file = files[i++];) {
    // const uploadObj = 
    uploadManager.add(++id, uploadType, file.fileName, file.fileSize)
  }
}
// 剥离外部状态
function Upload(uploadType) {
  this.uploadType = uploadType
}
Upload.prototype.delFile = function (id) {
  uploadManager.setExternalState(id, this) // 根据id给this绑定外部属性
  if (this.fileSize < 3000) {
    return this.dom.parentNode.removeChild(this.dom)
  }
  if (window.confirm('确定要删除该文件吗？' + this.fileName)) {
    return this.dom.parentNode.removeChild(this.dom)
  }
}

startUpload('plugin', [
  {
    fileName: '1.txt',
    fileSize: 1000
  },
  {
    fileName: '2.html',
    fileSize: 3000
  },
  {
    fileName: '3.txt',
    fileSize: 5000
  }
]);
startUpload('flash', [
  {
    fileName: '4.txt',
    fileSize: 1000
  },
  {
    fileName: '5.html',
    fileSize: 3000
  },
  {
    fileName: '6.txt',
    fileSize: 5000
  }
]);


/**
 * 享元模式带来的好处很大程度上取决于如何使用以及何时使用，一般来说，以下情况发生时便可以使用享元模式
 *  一个程序中使用了大量的相似的对象。
 *  由于使用了大量的对象，造成很大的内存开销。
 *  对象大多数状态都可以转为外部状态。
 *  剥离出对象的外部状态之后，可以用相对较少的共享对象取代大量的对象
 */


/*对象池
  对象池维护一个装载空闲对象的池子，如果需要对象的时候，不是直接new，而是转从对象池里获取。如果对象池里没有空闲的对象，则创建一个新的对象，当获取出的对象完成它的职责之后，再进入池子等待下次获取。

  对象池是另外一种性能优化方案，它跟享元模式有一些相似之处，但没有分离内部状态和外部状态这个过程。

  对象池的使用场景比较广泛，如http连接池，数据库连接池都是其代表应用。web前端开发中，对象池使用最多的场景就是和DOM相关的操作。
*/

const toolTipFactory = (function () {
  const toolTipPool = []
  return {
    create: function () {
      if (toolTipPool.length === 0) {
        const div = document.createElement('div')
        document.body.append(div)
        return div
      }
      console.log(toolTipPool)
      return toolTipPool.shift()
    },
    recover: function (toolTipDom) {
      console.log(toolTipDom)
      return toolTipPool.push(toolTipDom)
    }
  }
})()

const ary = []
for (let i = 0, str; str = ['A', 'B'][i++];) {
  const div = toolTipFactory.create()
  div.innerHTML = str
  ary.push(ary)
}

for (let i = 0, item; item = ary[i++];) {
  toolTipFactory.recover(item)
}

// 虽然执行了6次，但是由于对象池的缘故，只创建了4个，另外2个则是从对象池中获取的。
for (let i = 0,str; str = ['A','B','C','D','E','F'][i++];) {
  const div = toolTipFactory.create()
  div.innerHTML = str
}