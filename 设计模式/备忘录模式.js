// 棋子
class ChessPieces {
  constructor(){
    this.chess = {}
  }
  // 获取棋子
  getChess(){
    return this.chess
  }
}
// 记录棋路
class Record {
  constructor(){
    this.chessTallyBook = [] // 记录棋路
  }
  recordTallyBook(chess){
    // console.log(this.chessTallyBook.includes(chess))
    const isLoadtion = this.chessTallyBook.some(
      item=>item.location === chess.location
    )
    if(isLoadtion){
      console.log(`${chess.type},${chess.location}已存在其他棋子`)
    }else {
      this.chessTallyBook.push(chess)
    }
    // this.chessTallyBook.some(item=>item.location === chess.location)
  }
  getTallyBook(){
    return this.chessTallyBook.pop()
  }
}

// 下棋规则
class ChessRule {
  constructor(){
    this.chessInfo = {}
  }
  playChess(chess){
    this.chessInfo = chess
  }
  getChess(){
    return this.chessInfo
  }
  // 记录棋路
  recordTallyBook(){
    return new ChessPieces(this.chessInfo)
  }
  // 悔棋
  repentanceChess(chess){
    this.chessInfo = chess.getTallyBook()
  }
}

const chessRule = new ChessRule()
const record = new Record()

chessRule.playChess({
  type:'黑棋',
  location:'X10,Y10'
})
record.recordTallyBook(chessRule.getChess())//记录棋路
chessRule.playChess({
  type:'白棋',
  location:'X11,Y10'
})
record.recordTallyBook(chessRule.getChess())//记录棋路
chessRule.playChess({
  type:'黑棋',
  location:'X11,Y11'
})
record.recordTallyBook(chessRule.getChess())//记录棋路
chessRule.playChess({
  type:'白棋',
  location:'X12,Y10'
})
console.log(chessRule.getChess())//{type:'白棋',location:'X12,Y10'}
chessRule.repentanceChess(record) // 悔棋
console.log(chessRule.getChess())//{type:'黑棋',location:'X11,Y11'}
chessRule.repentanceChess(record) // 悔棋
console.log(chessRule.getChess())//{type:'白棋',location:'X11,Y10'}
// // 棋子
// class ChessPieces  {
//   constructor(chess){
//     this.chess = chess
//   }
//   getChess(){
//     return this.chess
//   }
// }
// // 记录每一步棋路的小本本
// class RecordChessPieces {
//   constructor(){
//     this.chesstallyBook = []
//   }
//   playChessPieces(chessPieces){
//     this.chesstallyBook.push(chessPieces)
//     // if(this.chesstallyBook.includes(chessPieces)){
//     //   throw new Error('该位置已经存在其他棋子了')
//     // }
//   }
//   getChessPieces(){
//     this.chesstallyBook.splice(this.chesstallyBook.length-1,1)
//     console.log(this.chesstallyBook)
//     return this.chesstallyBook[this.chesstallyBook.length-1]
//   }
// }
// // 下棋的规则
// class ChessRule {
//   constructor(){
//     this.content = null
//   }
//   setChess(content){
//     this.content = content
//   }
//   getChess(){
//     return this.content
//   }
//   // 记录棋子的棋路，只有被记录的棋路才能悔棋
//   chessCheckIn(){
//     return new ChessPieces(this.content)
//   }
//   // 执行悔棋
//   repentanceChess(chessPieces){
//     this.content = chessPieces.getChess()
//   }
// }

// // 下棋的规则
// let chessRule = new ChessRule()
// // 备忘录
// let recordChessPieces = new RecordChessPieces()
 
// chessRule.setChess({
//   type:'白棋',
//   location:'x10-y10'
// })
 
// recordChessPieces.playChessPieces(chessRule.chessCheckIn()) 
// chessRule.setChess({
//   type:'黑棋',
//   location:'x9-y9'
// })
// chessRule.setChess({
//   type:'白棋',
//   location:'x11-y11'
// })
// recordChessPieces.playChessPieces(chessRule.chessCheckIn()) 

// console.log(chessRule.getChess())
// chessRule.repentanceChess(recordChessPieces) // 撤销
// chessRule.repentanceChess(recordChessPieces) // 撤销
// chessRule.repentanceChess(recordChessPieces) // 撤销
// console.log(chessRule.getChess())
// // editor.getContentFromMemento(careTaker.get(0)) // 撤销
// // console.log(editor.getContent())