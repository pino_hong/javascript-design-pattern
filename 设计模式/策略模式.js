//策略模式指的是定义一系列的算法，把他们一个个封装起来，策略模式的目的就是将算法的使用与算法的实现分离开来。
// 策略模式还可以用来封装一系列业务规则，只要这些业务规则指向的目标一致，并且可以被替换使用，就可以用策略模式来封装它们。
// 在javascript将函数作为一等公民的语言里，策略模式就是隐形的，它已经融入到了语言的本身当中，如：我们经常用的高阶函数封装不同的行为，并且将它们传递到另一个函数中。

/**
 * 使用策略模式计算奖金
 */
/*
// 定义一系列算法，把他们各自封装成策略类，算法被封装在策略类内部的方法里。
const performanceS = function () { }
performanceS.prototype.calculate = function (salary) {
  return salary * 4
}

const performanceA = function () { }
performanceA.prototype.calculate = function (salary) {
  return salary * 3
}

const Bonus = function(){
  this.salary = null // 原始工资
  this.strategy = null // 计效对应的策略模式
}
// 设置员工的原始工资
Bonus.prototype.setSalary = function(salary){
  this.salary = salary
}
// 设置员工绩效等级对应的策略对象
Bonus.prototype.setStrategy = function(strategy){
  this.strategy = strategy
}
// 获取奖金数额
Bonus.prototype.getBinus = function(){
  return this.strategy.calculate(this.salary)
}

const bonus = new Bonus()
bonus.setSalary(10000)
bonus.setStrategy(new performanceA())//设置策略对象

console.log(bonus.getBinus())

*/

/**
 * javascript中的策略模式
 * 
 */

/*
const strategies = {
  "S":function(salary){
    return salary*4
  },
  "A":function(salary){
    return salary*3
  }
}

const calculateBonus = function(type,salary){
  return strategies[type](salary)
}
console.log(calculateBonus('S',12000))
*/

/**
 * 多态在策略模式中的体现
 * 
 * 通过使用的策略模式重构的代码，我们消除了程序中大量的条件分支语法。所有的算法使用逻辑(计算奖金)分布在各个策略对象中。
 */
// 检验逻辑封装为策略对象
/*******************
const strategies = {
  isNonEmpty: function (value, errorMsg) {
    if (!value) {
      return errorMsg
    }
  },
  minLength: function (value, length, errorMsg) {
    if (value.length < length) {
      return errorMsg
    }
  },
  isMobile: function (value, errorMsg) {
    if (!/(^1[3|5|8][0-9]{9}$)/.test(value)) {
      return errorMsg
    }
  }
}
// 验证器
function Validator() {
  this.cache = [] // 保存检验规则
}
************************************/
/* 验证单个规则写法 */
// Validator.prototype.add = function (dom, rule, errorMsg) {
//   var ary = rule.split(':') // strategy与参数分开
//   this.cache.push(function () {// 检验规则用空函数包装起来
//     const strategy = ary.shift() // 获取strategy，检验规则的key
//     ary.unshift(dom.value) // input的value值插入参数列表的首位
//     ary.push(errorMsg) // 错误信息插入参数的末尾
//     return strategies[strategy].apply(dom, ary) // 返回指定的检验规则，并指定dom作为它的指针，input的value值、value长度，错误信息指定为它的参数。
//   })
// }
/**验证多个规则写法 */
/*
Validator.prototype.add = function (dom, rules) {
  const _this = this
  for (let i = 0, rule; rule = rules[i++];) {
    (function (rule) {
      const strategyAry = rule.strategy.split(':')
      _this.cache.push(function () {
        const strategy = strategyAry.shift()
        strategyAry.unshift(dom.value)
        strategyAry.push(rule.errorMsg)
        return strategies[strategy].apply(dom, strategyAry)
      })
    })(rule)
  }
  console.log(this.cache,'_this.cache')
}
// 通过遍历执行cache里面的所有存储的方法，如果存在错误信息则中断循环，返回错误信息。
Validator.prototype.start = function () {
  for (let i = 0, validatorFunc; validatorFunc = this.cache[i++];) {
    var msg = validatorFunc()
    if (msg) {
      return msg
    }
  }
}
const registerForm = document.getElementById('registerForm') // 获取formDom节点

function validataFunc() {
  const validator = new Validator() // 创建一个Validator，通过它调用添加规则，以及执行存储规则
  // 添加验证规则
  // validator.add(registerForm.userName, 'isNonEmpty', '用户名不能为空')
  // validator.add(registerForm.password, 'minLength:6', '密码长度不能少于6位')
  // validator.add(registerForm.phoneNumber, 'isMobile', '手机号码格式不对')

  // 多个校验规则
  validator.add(registerForm.userName, [
    {
      strategy: 'isNonEmpty',
      errorMsg: '用户名不能为空'
    }, {
      strategy: 'minLength:10',
      errorMsg: '密码长度不能少于10位'
    }
  ])

  validator.add(registerForm.password, [{
    strategy: 'minLength:6',
    errorMsg: '密码长度不能少于6位'
  }])

  validator.add(registerForm.phoneNumber, [{
    strategy: 'isMobile',
    errorMsg: '手机号码格式不对'
  }])

  const errorMsg = validator.start()
  return errorMsg // 返回错误信息。
}

registerForm.onsubmit = function () {
  const errorMsg = validataFunc()
  if (errorMsg) { // 如果存在错误信息，显示错误信息，并且阻止onsubmit默认事件
    alert(errorMsg)
    return false
  }
}
*/
// 通过以上的策略模式校验表单，仅仅通过配置的方式就可以实现一个表单的校验。能作为插件的形式，扩展性强，这些校验规则适用任何地方。

/**
 * 策略模式的优缺点：
 *  优点：
 *    策略模式利用组合，委托和多态等技术思想，可以有效地避免多重条件选择语句
 *    通过将算法独立的封装，使得它易理解，易切换，易扩展。
 *  缺点：
 *    策略模式会创建出很多策略类或者策略对象，但是这些可以忽略不计，比起把他们堆砌在context要好得多。
 *    策略模式要理解所有的策略类/对象里面的规则，针对不同规则做出不同的处理执行。  
 */

/**
 * @t 动画已消耗的时间
 * @b 小球原始位置
 * @c 小球的目标位置
 * @d 动画持续总时间
 */
const tween = {
  linear: function (t, b, c, d) {
    return c * t / d + b;
  },
  easeIn: function (t, b, c, d) {
    return c * (t /= d) * t + b;
  },
  strongEaseIn: function (t, b, c, d) {
    return c * (t /= d) * t * t * t * t + b;
  },
  strongEaseOut: function (t, b, c, d) {
    return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
  },
  sineaseIn: function (t, b, c, d) {
    return c * (t /= d) * t * t + b;
  },
  sineaseOut: function (t, b, c, d) {
    return c * ((t = t / d - 1) * t * t + 1) + b;
  }
};
// 动画类  传入的dom为即将运动起来的dom
function Animate(dom) {
  this.dom = dom // 进行运动的dom节点
  this.startTime = 0 // 动画开始时间
  this.startPos = 0 // 动画开始时，dom节点的位置，即初始位置
  this.endPos = 0 // 动画结束时，dom节点的位置，即dom的目标位置
  this.propertyName = null // dom节点需要被改变的css属性名
  this.easing = null // 缓动算法
  this.duration = null // 动画持续时间
}
// 表示元素每一帧要做的事件
Animate.prototype.step = function () {
  const t = +new Date //当前时间戳
  // 当前时间如果大于动画开始时间+动画总时长执行
  if (t >= this.startTime + this.duration) {
    console.log(t, this.startTime, this.duration)
    this.update(this.endPos)
    return false // 返回false表示结束了
  }
  const pos = this.easing(t - this.startTime, this.startPos, this.endPos - this.startPos, this.duration)
  this.update(pos)
}

Animate.prototype.start = function (propertyName, endPos, duration, easing) {
  this.startTime = +new Date // 获得当前时间戳
  /**
   * ele.getBoundingClientRect()返回元素大小及其相对于视口的位置
   * 如果是标准盒子模型：元素的尺寸等于 width/height +padding+border-width的总和
   * 如果是box-sizing:border-box,元素的尺寸等于width/height
   */
  // 根据propertyName获取节点的初始位置。
  this.startPos = this.dom.getBoundingClientRect()[propertyName]
  this.propertyName = propertyName // dom节点需要被改变的css属性名，如top,left
  this.endPos = endPos // dom节点的目标位置，移动多少px
  this.duration = duration // 动画持续时间
  this.easing = tween[easing] // 使用哪种缓动算法

  const _this = this
  let timeId = setInterval(function () { // 开启计时器
    if (_this.step() === false) {//动画达到终点，则结束计时器
      clearInterval(timeId)
    }
  }, 19)
}


// 根据propertyName跟新dom的属性位置
Animate.prototype.update = function (pos) {
  this.dom.style[this.propertyName] = pos + 'px'
}

// const div = document.getElementById('animateDiv')
// const animate = new Animate(div)

// animate.start('left', 500, 1000, 'strongEaseOut')

class Strategies {
  constructor() {
    this.rules = {}
  }
  add(key, rule) {
    this.rules[key] = rule
    return this
  }
}

class Validator {
  constructor(strategies) {
    this.cache = [] // 保存检验规则
    this.strategies = strategies
  }
  add(dom, rules) {
    rules.forEach((rule) => {
      const strategyAry = rule.strategy.split(':')
      this.cache.push(() => {
        const strategy = strategyAry.shift()
        strategyAry.unshift(dom.value)
        strategyAry.push(rule.errorMsg)
        console.log(this.strategies[strategy])
        return this.strategies[strategy].apply(dom, strategyAry)
      })
    });
  }
  start() {
    for (let i = 0,validatorFunc; validatorFunc =this.cache[i++]; ) {
      const msg = validatorFunc()
      if (msg) {
        return msg
      }
    }
  }
}
const registerForm = document.getElementById('registerForm') // 获取formDom节点
const strategies = new Strategies()
strategies.add('isNonEmpty', function(value, errorMsg) {
  if (!value) {
    return errorMsg
  }
}).add('minLength', function(value, length, errorMsg) {
  if (value.length < length) {
    return errorMsg
  }
}).add('isMobile', function(value, errorMsg) {
  if (!/(^1[3|5|8][0-9]{9}$)/.test(value)) {
    return errorMsg
  }
})
function validataFunc() {
  const validator = new Validator(strategies.rules)
  // 多个校验规则
  validator.add(registerForm.userName, [
    {
      strategy: 'isNonEmpty',
      errorMsg: '用户名不能为空'
    }, {
      strategy: 'minLength:10',
      errorMsg: '用户名长度不能少于10位'
    }
  ])
  validator.add(registerForm.password, [{
    strategy: 'minLength:6',
    errorMsg: '密码长度不能少于6位'
  }])
  validator.add(registerForm.phoneNumber, [{
    strategy: 'isMobile',
    errorMsg: '手机号码格式不对'
  }])

  const errorMsg = validator.start()
  return errorMsg // 返回错误信息。
}
registerForm.onsubmit = function () {
  const errorMsg = validataFunc()
  if (errorMsg) { // 如果存在错误信息，显示错误信息，并且阻止onsubmit默认事件
    console.log(errorMsg)
    return false
  }
}