// 组合模式就是由一些小的子对象来构建更大的对象，而这些小的子对象本身也许是由更多的小的“孙对象”构建出来的。

/** 组合模式的用途
 * 组合模式将对象组合成树状结构，以表示“部分-整体”的层次结构。除了用来表示树状结构之外，组合模式的另一个好处就是通过对象的多态性表现，使得用户对单个对象和组合对象的使用具有一致性。下面对此做出说明：
 *  1.表示树状结构
 *  2.利用对象多态性统一对待组合对象和单个对象
 */

//在javascript中实现组合模式的难点在于要保证组合对象和叶对象对象拥有同样的方法，这通常需要用鸭子类型的思想对他进行接口检查。

function MacroCommand() {
  return {
    commandList: [],
    add: function (command) {
      this.commandList.push(command)
    },
    execute: function () {
      for (let i = 0, command; command = this.commandList[i++];) {
        command.execute()
      }
    }
  }
}

const openAcCommand = {
  execute: function () {
    console.log('打开空调')
  },
  // 组合模式的透明性带来的问题，可能会出现误操作，如给叶对象添加子节点，叶对象就是顶点了，这从逻辑上肯定是不合理的，因此处理给叶对象添加节点抛出异常
  add: function () {
    throw new Error('不能向叶对象中添加子节点')
  }
}
//-----------------------
const openTvCommand = {
  execute: function () {
    console.log('打开电视')
  }
}

const openSoundCommand = {
  execute: function () {
    console.log('打开音响')
  }
}
const macroCommand1 = MacroCommand()
macroCommand1.add(openTvCommand)
macroCommand1.add(openSoundCommand)
// -----------------------
const closeDoorCommand = {
  execute: function () {
    console.log('关门')
  }
}

const openPcCommand = {
  execute: function () {
    console.log('开电脑')
  }
}

const openQQCommand = {
  execute: function () {
    console.log('打开QQ')
  }
}
const macroCommand2 = MacroCommand()
macroCommand2.add(closeDoorCommand)
macroCommand2.add(openPcCommand)
macroCommand2.add(openQQCommand)

const macroCommand = MacroCommand()
macroCommand.add(openAcCommand)
macroCommand.add(macroCommand1)
macroCommand.add(macroCommand2)
document.getElementById('button').onclick = function () {
  console.log(macroCommand.commandList)
  macroCommand.execute()
}

// 组合模式的例子-扫描文件夹
// 文件夹和文件之间的关系，非常适合用组合模式来描述。文件夹可以包括文件夹和文件，这种关系刚好可以组成一棵树。
// 文件夹类
/*
function Folder(name) {
  this.name = name
  this.files = []
}
Folder.prototype.add = function (file) {
  this.files.push(file)
}
Folder.prototype.scan = function () {
  console.log('开始扫码文件夹：' + this.name)
  for (let i = 0, file; file = this.files[i++];) {
    file.scan()
  }
}
// 文件类
function File(name) {
  this.name = name
}
File.prototype.add = function () {
  throw new Error('文件下面不能再添加文件')
}
File.prototype.scan = function () {
  console.log('开始扫描文件：' + this.name)
}

const folder = new Folder('学习资料')
const folder1 = new Folder('javascript')
const folder2 = new Folder('JQ')
const folder4 = new Folder('Node.js')

const file1 = new File('javascript设计模式')
const file2 = new File('精通JQ')
const file3 = new File('重构与模式')
const file4 = new File('深入浅出Node.js')
const file5 = new File('javascript语言精髓与编程实践')

folder1.add(file1)
folder2.add(file2)
folder4.add(file4)

folder.add(folder1)
folder.add(folder2)
folder.add(folder4)
folder.add(file3)
folder.add(file5)

folder.scan()
*/
/** 组合模式需要注意的地方
 * 
 * 1.组合模式不是父子关系，而是一种聚合的关系。组合对象把请求委托给它所包含的所有叶对象，它们能够合作的关键是拥有相同的接口。
 * 
 * 2. 对叶对象操作的一致性
 *    组合模式除了要求组合对象和叶对象拥有相同的接口之外，还有一个必要的条件，就是对一组叶对象的操作必须具有一致性。
 * 
 * 3. 双向映射关系。
 *    有些叶对象可能属入多个子对象，这种复合情况下我们必须给父节点和子节点建立双向映射关系，简单的做法就是给小组和员工对象都增加集合来保存对方的引用。
 * 
 * 4.用职责链模式提高组合模式性能
 *  在组合模式中，树的结构比较复杂，节点数量很多的情况下进行遍历树的结构，会造成性能方面不够理想。这个时候通过一些技巧，避免遍历整棵树，如借助职责链模式。
 */

// 引用父对象

/*
function Folder(name) {
  this.name = name
  this.parent = null
  this.files = []
}

Folder.prototype.add = function (file) {
  file.parent = this
  this.files.push(file)
}

Folder.prototype.scan = function () {
  console.log(`开始扫描文件夹：${this.name}`)
  for (let i = 0, file, files = this.files; file = files[i++];) {
    file.scan()
  }
}

Folder.prototype.remove = function () {
  if (!this.parent) {
    return
  }
  console.log(this.parent)
  for (let files = this.parent.files, i = files.length - 1; i >= 0; i--) {
    const file = files[i]
    if (file === this) {
      files.splice(i, 1)
      break
    }
  }
}

function File(name) {
  this.name = name
  this.parent = null
}

File.prototype.add = function () {
  throw new Error('不能添加在文件下面')
}
File.prototype.scan = function () {
  console.log('开始扫描文件' + this.name)
}
File.prototype.remove = function () {
  if (!this.parent) {
    return
  }
  for (let files = this.parent.files, i = files.length - 1; i >= 0; i++) {
    const file = files[i]
    if (file === this) {
      files.splice(i, 1)
    }
  }
}

var folder = new Folder('学习资料');
var folder1 = new Folder('JavaScript');
var file1 = new Folder('深入浅出 Node.js');
folder1.add(new File('JavaScript 设计模式与开发实践'));
folder.add(folder1);
folder.add(file1);
folder1.remove(); //移除文件夹
folder.scan();

console.log(folder)
*/

// 文件夹类
class Folder {
  constructor(name) {
    this.name = name
    this.parent = null;
    this.files = []
  }
  add(file) {
    file.parent = this
    this.files.push(file)
    return this
  }
  scan() {
    console.log(`开始扫描文件夹：${this.name}`)
    this.files.forEach(file => {
      file.scan()
    });
  }
  remove() {
    if (!this.parent) {
      return
    }
    for (let files = this.parent.files, i = files.length - 1; i >= 0; i--) {
      const file = files[i]
      if (file === this) {
        files.splice(i, 1)
        break
      }
    }
  }
}
// 文件类
class File {
  constructor(name) {
    this.name = name
    this.parent = null
  }
  add() {
    throw new Error('文件下面不能添加任何文件')
  }
  scan() {
    console.log(`开始扫描文件：${this.name}`)
  }
  remove() {
    if (!this.parent) {
      return
    }
    for (let files = this.parent.files, i = files.length - 1; i >= 0; i++) {
      const file = files[i]
      if (file === this) {
        files.splice(i, 1)
      }
    }
  }
}

const book = new Folder('电子书')
const js = new Folder('js')
const node = new Folder('node')
const vue = new Folder('vue')
const js_file1 = new File('javascript高级程序设计')
const js_file2 = new File('javascript忍者秘籍')
const node_file1 = new File('nodejs深入浅出')
const vue_file1 = new File('vue深入浅出')

const designMode = new File('javascript设计模式实战')

js.add(js_file1).add(js_file2)
node.add(node_file1)
vue.add(vue_file1)

book.add(js).add(node).add(vue).add(designMode)
js.remove()
book.scan()


