/**
 * 命令模式是最简单和优雅的模式之一，命令模式中的命令指的是一个执行某些特定事情的指令。
 * 命令模式最常见的应用场景是：有时候需要向某些对象发送请求，但是并不知道请求的接受着是谁，也不知道被请求的操作是什么。此时通过一种松耦合的方式来设计程序，使得请求发送者和请求接收者能够消除彼此之间的耦合关系。
 */

const button1 = document.getElementById('button1'),
  button2 = document.getElementById('button2'),
  button3 = document.getElementById('button3');
  const MenBar = {
    refresh:function(){
      console.log('刷新菜单目录')
    }
  }
  const SubMenu = {
    add:function(){
      console.log('增加子菜单')
    },
    del:function(){
      console.log('删除子菜单')
    }
  }
  function setCommand(el,command){
    el.onclick = function(){
      command.execute()
    }
  }
  
  class MenuBarCommand{
    constructor(receiver,key){
      this.receiver = receiver
      this.key = key
    }
    execute(){
      this.receiver[this.key]()
    }
  }
  setCommand(button1,new MenuBarCommand(MenBar,'refresh'))
  setCommand(button2,new MenuBarCommand(SubMenu,'add'))
  setCommand(button3,new MenuBarCommand(SubMenu,'del'))
// 命令模式的例子 - 菜单程序
/*
function setCommand(el,command){
  el.onclick = function(){
    command.execute()
  }
}

const MenBar = {
  refresh:function(){
    console.log('刷新菜单目录')
  }
}

const SubMenu = {
  add:function(){
    console.log('增加子菜单')
  },
  del:function(){
    console.log('删除子菜单')
  }
}

function RefreshMenuBarCommand(receiver){
  this.receiver = receiver
}
RefreshMenuBarCommand.prototype.execute = function(){
  this.receiver.refresh()
}
function AddSubMenuCommand(receiver){
  this.receiver = receiver
}
AddSubMenuCommand.prototype.execute = function(){
  this.receiver.add()
}
function DelSubMenuCommand(receiver){
  this.receiver = receiver
}
DelSubMenuCommand.prototype.execute = function(){
  this.receiver.del()
}

setCommand(button1,new RefreshMenuBarCommand(MenBar))
setCommand(button2,new AddSubMenuCommand(SubMenu))
setCommand(button3,new DelSubMenuCommand(SubMenu))
*/

// javascript中的命令模式
// javascript作为将函数作为一等对象的语言，跟策略模式一样，命令模式也早已融入到了javaScript语言之中。因此在javacript中使用命令模式相比传统面向对象语言的命令模式要简单的多。
/*
function setCommand(btn, func) {
  btn.onclick = function () {
    console.log(func)
    func.execute()
  }
}

const MenuBar = {
  refresh: function () {
    console.log('刷新菜单界面')
  }
}

function RefreshMenuBarCommand(receiver) {
  return function () {
    receiver.refresh()
  }
}

const refreshMenuBarCommand = RefreshMenuBarCommand(MenuBar)
setCommand(button1, refreshMenuBarCommand)
*/
// 撤销命令
// 撤销命令能够非常轻易的实现围棋中的悔棋功能，以及文本编辑器的ctrl+z撤回功能。
const ball = document.getElementById('ball');
const pos = document.getElementById('pos');
const moveBtn = document.getElementById('moveBtn');
const cancelBtn = document.getElementById('cancelBtn')
// moveBtn.onclick = function () {
//   console.log(pos.value)
//   const animate = new Animate(ball)
//   animate.start('left', pos.value, 1000, 'strongEaseOut')
// }

function MoveCommand(receiver, pos) {
  this.receiver = receiver
  this.pos = pos
  this.oldPos = null
}

MoveCommand.prototype.execute = function () {
  this.receiver.start('left', this.pos, 1000, 'strongEaseOut')
  this.oldPos = this.receiver.dom.getBoundingClientRect()[this.receiver.propertyName]
  console.log(this.oldPos)
}
MoveCommand.prototype.undo = function () {
  this.receiver.start('left', this.oldPos, 1000, 'strongEaseOut')
}
let moveCommand
moveBtn.onclick = function () {
  const animate = new Animate(ball)
  moveCommand = new MoveCommand(animate, pos.value)
  moveCommand.execute()
}

cancelBtn.onclick = function () {
  moveCommand.undo() // 撤销命令
}

// 撤销和重做
// 模仿游戏中角色动作以及播放录像功能
const Ryu = {
  attack: function () {
    console.log('攻击');
  },
  defense: function () {
    console.log('防御');
  },
  jump: function () {
    console.log('跳跃');
  },
  crouch: function () {
    console.log('蹲下');
  }
}
function makeCommand(receiver, state) {
  return function () {
    receiver[state]()
  }
}
const commands = {
  "w": "jump", // W
  "s": "crouch", // S
  "a": "defense", // A
  "d": "attack" // D
}

const commandStack = []
// 
document.onkeydown = function (ev) {
  if (!commands[ev.key]) {
    return
  }
  const keyCode = ev.key,
    command = makeCommand(Ryu, commands[keyCode])
  command()
  if (command) {
    commandStack.push(command)
  }
}
// 录像功能
document.getElementById('replay').onclick = function () {
  let command
  while (command = commandStack.shift()) {
    command()
  }
}

// 宏命令
// 宏命令是一组命名的集合，通过执行宏命令的方式，可以一次执行一批命令。

const closeDoorCommand = {
  execute: function () {
    console.log('关门');
  }
};
const openPcCommand = {
  execute: function () {
    console.log('开电脑');
  }
};
const openQQCommand = {
  execute: function () {
    console.log('登录 QQ');
  }
};

function MacroCommand(){
  return {
    commandList:[],
    add:function(command){
      this.commandList.push(command)
    },
    execute:function(){
      for(let i = 0,fn;fn = this.commandList[i++];){
        fn.execute()
      }
    }
  }
}

const macroCommand = MacroCommand()
macroCommand.add(closeDoorCommand)
macroCommand.add(openPcCommand)
macroCommand.add(openQQCommand)

macroCommand.execute()
