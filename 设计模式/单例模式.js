// 单例模式的定义是：保证一个类不论执行几次或者实例几次永远只有一个实例，并提供一个访问它的全局访问点
// 使用场景比较多，如线程池，全局缓存，浏览器的window对象。以及某个弹窗的实现
// 实现思路：在第一次执行或者实例化时，验证缓存里是否存在，如果不存在存入缓存中并且返回，如果存在直接从缓存中返回

/* 
  以下为单例模式最简单的写法，仅仅验证单例模式的可行性。
  缺点就是不透明性，使用者必须知道它是一个单例类，并且通过函数下面的静态方法去获取，与以往的new XXX方式获取对象不同。
*/
function Singleton(name) {
  this.name = name
  // this.instance = null
}
Singleton.prototype.getName = function () {
  console.log(this.name)
}
/*
Singleton.getInstance = function(name){
  if(!this.instance){
    this.instance = new Singleton(name)
  }
  return this.instance
}
*/
// 又或者另一种写法
/*
Singleton.getInstance = (function(){
  var instance = null
  return function(name){
    if(!instance){
      console.log(123)
      instance = new Singleton(name)
    }
    return instance
  }
})()
const a = Singleton.getInstance('张三')
const b = Singleton.getInstance('李四')
*/

/* 
  实现一个透明的单例模式，让用户像使用其他普通类一样的单例模式 
  缺点：虽然实现了透明的单例模式，但是其构造函数中做的事情不够单一，如果后期让他可以创建多个节点，需要修改构造函数的内部，因此它不够灵活。
*/
// 创建页面中唯一的节点。
/*
const CreateDiv = (function(){
  let instance = null
  const createDiv = function(html){
    if(instance){
      return instance
    }
    this.html = html
    this.init();
    return instance = this
  }
  createDiv.prototype.init = function(){
    const div = document.createElement('div')
    div.innerHTML = this.html
    document.body.appendChild(div)
  }

  return createDiv
})()

const a = new CreateDiv('zhangsan')
const b = new CreateDiv('lisi')
console.log(a === b,a)
*/

/**
 * 用代理实现代理模式
 * 思路：将负责单例的逻辑抽离出来，放在一个代理类中。每次去实例化这个代理类。这样createDiv就成了一个普通的类。它们之间的组合解决了上述问题。
 */

function CreateDiv(html) {
  this.html = html
  this.init()
}

CreateDiv.prototype.init = function () {
  const div = document.createElement('div')
  div.innerHTML = this.html
  document.body.appendChild(div)
}

// 代理类  将处理单例的逻辑分离出来
const ProxySingletonCreateDiv = (function () {
  let instance = null
  return function (html) {
    if (!instance) {
      instance = new CreateDiv(html)
    }
    return instance
  }
})();
const a = new ProxySingletonCreateDiv('zhansgan---')
const b = new ProxySingletonCreateDiv('lisi')
console.log(a, a === b)

/**
 * javascript中的单例模式
 * javascript不同于其他语言由类创建唯一的对象，它是一门无类的语言，基于类的单例模式在javascript中并不适用。生搬单例模式概念没有意义。在javascript创建对象或者唯一对象非常简单。
 */
/*
var MyApp = {}
MyApp.namespace = function (name) {
  const parts = name.split('.')
  // console.log(parts)
  let current = MyApp
  for (const i in parts) {
    // console.log(i)
    if (!current[parts[i]]) {
      current[parts[i]] = {}
    }
    // current:{
    //   event:{}
    //   dom:{}
    //   style:{}
    // }
    console.log(current[parts[i]])
    current = current[parts[i]]
    console.log(current)
  }
}

MyApp.namespace('event')
MyApp.namespace('dom.style')
console.dir(MyApp)
*/

/**
 * 惰性单例
 * 它表示的是只在需要的时候被创建，而不是一开始加载好就创建。
 */
/*
const createLoginLayer = (function(){
  let div
  return function(){
    if(!div){
      div = document.createElement('div')
      div.innerHTML = '登录界面'
      div.style.display = 'none'
      document.body.appendChild(div)
    }
    console.log(111)
    return div
  }
})()

document.body.onclick = function(){
  const loginLayer = createLoginLayer()
  loginLayer.style.display = 'block'
}
*/
/**
 * 通用惰性单例
 * 在上面的惰性单例还是存在一个问题，就是单例逻辑和创建对象逻辑没有分离，如果创建对象的逻辑需求发生了变化，就需要更改createLoginLayer代码了，这显然是不好的。
 */
// 单独剥离出单例逻辑
// const getSingle = function(fn){
//   let result
//   return function(){
//     return result || (result = fn.apply(this,arguments))
//   }
// }

// const createDiv2 = function(){
//   const div = document.createElement('div')
//   div.innerHTML = '弹窗'
//   div.style.display = 'none'
//   document.body.appendChild(div)
//   return div
// }

// const creteSingleLoginLayer = getSingle(createDiv2)

// document.body.onclick = function(){
//   const _creteSingleLoginLayer = creteSingleLoginLayer()
//   _creteSingleLoginLayer.style.display = 'block'
// }

class CreateDiv3 {
  constructor(html){
    if(!CreateDiv3.instance){
      CreateDiv3.instance = this.init(html)
    }
    return CreateDiv3.instance
  }

  init(html){
    const div = document.createElement('div')
    div.innerHTML = html
    document.body.appendChild(div)
    return div
  }
}
let a2 = new CreateDiv3('html10')
let b2 = new CreateDiv3('html2')
console.log(new CreateDiv3() === b2)
console.log(CreateDiv3.instance)