/**
 * 具体表现的四个角色
 * 产品：建造的产物
 * 导演：指挥建造的过程，不涉及建造的细节
 * 建造者：抽象建造过程，规定产品哪些部分需要创建
 * 具体建造者：实现建造者，实现产品各个部分的建造，并提供产品
 * 
 * 以服装厂为例
 * 产品：衣服  各种衣服
 * 导演：销售员
 * 建造者：厂长
 * 具体建造者：工人
 */
// 
class Clothing {
  constructor() {
    this.clothingType = ''
    this.price
  }
}

class Underwear extends Clothing {
  constructor() {
    super()
    this.clothingType = 'underwear'
    this.price = 10
  }
}

class TShirt extends Clothing {
  constructor() {
    super()
    this.clothingType = 't_shirt'
    this.price = 50
  }
}

class DownCoat extends Clothing {
  constructor() {
    super()
    this.clothingType = 'DownCoat'
    this.price = 500
  }
}
// 产品
class Purchase {
  constructor() {
    this.clothings = []
  }
  addClothing(clothing) {
    this.clothings.push(clothing)
  }
  countPrice() {
    return this.clothings.reduce((prev, cur)=>cur.price + prev,0)
  }
}

// 厂长
class FactoryManager {
  createUnderwear() {
    throw new Error(`子类必须重写 createUnderwear`)
  }
  createTShirt() {
    throw new Error(`子类必须重写 createTShirt`)
  }
  createDownCoat() {
    throw new Error(`子类必须重写 DownCoat`)
  }
}

// 工人
class Worker extends FactoryManager {
  constructor() {
    super()
    this.purchase = new Purchase()
  }
  createUnderwear(num) {
    for (let i = 0; i < num; i++) {
      this.purchase.addClothing(new Underwear())
    }
  }
  createTShirt(num) {
    for (let i = 0; i < num; i++) {
      this.purchase.addClothing(new TShirt())
    }
  }
  createDownCoat(num) {
    for (let i = 0; i < num; i++) {
      this.purchase.addClothing(new DownCoat())
    }
  }
}

// 销售
class Salesman {
  constructor() {
    this.worker = null
  }
  setWorker(worker) {
    this.worker = worker
  }
  reserve(clothing) {
    clothing.forEach((item) => {
      if (item.type === 'underwear') {
        this.worker.createUnderwear(item.num)
      } else if (item.type === 't_shirt') {
        this.worker.createTShirt(item.num)
      } else if (item.type === 'DownCoat') {
        this.worker.createDownCoat(item.num)
      } else {
        try {
          throw new Error('公司暂不生产或不存在该类型的商品')
        } catch (error) {
          console.log(error)
        }
      }
    });

    const purchase = this.worker.purchase
    return purchase.countPrice()
  }
}

const salesman = new Salesman()
const worker = new Worker()
salesman.setWorker(worker)
const order = [
  {
    type: 'underwear',
    num: 10
  },
  {
    type: 't_shirt',
    num: 4
  },
  {
    type: 'DownCoat',
    num: 1
  }
]
console.log(`本次订单所需金额：${salesman.reserve(order)}`)