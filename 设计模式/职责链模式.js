/**
 * 职责链模式的定义是：使用多个对象都有机会处理请求，从而避免请求的发送者和接收者之间的耦合关系，将这些对象链成一条链，并沿着这条链传递该请求，直到有一个对象处理它为止。
 * 
 * 职责链模式的名字非常形象，一系列可能会处理请求的对象被连接成一条链，请求在这些对象之间依此传递，直到遇到一个可以处理它的对象，我们把这些对象称为链中的节点。
 */

function order500(orderType, pay, stock) {
  if (orderType === 1 && pay === true) {
    console.log('500元订金预约，得到100元优惠卷')
  } else {
    return 'nextSuccessor'
  }
}
function order200(orderType, pay, stock) {
  if (orderType === 2 && pay === true) {
    console.log('200元订金预约，得到50元优惠卷')
  } else {
    return 'nextSuccessor'
  }
}
function orderNormal(orderType, pay, stock) {
  if (stock > 0) {
    console.log('普通购买，无优惠卷')
  } else {
    console.log('手机库存不足')
  }
}

function Chain(fn) {
  this.fn = fn
  this.successor = null
}
Chain.prototype.setNextSuccessor = function (successor) {
  // console.log(this)
  return this.successor = successor
}
Chain.prototype.passRequest = function () {

  const ret = this.fn.apply(this, arguments)
  if (ret === 'nextSuccessor') {
    console.log(this.successor)
    return this.successor && this.successor.passRequest.apply(this.successor, arguments)
  }
  return ret
}
Chain.prototype.next = function () {
  return this.successor && this.successor.passRequest.apply(this.successor, arguments)
}
/*
const fn1 = new Chain(function () {
  console.log(1)
  return 'nextSuccessor'
})
const fn2 = new Chain(function () {
  console.log(2)
  setTimeout(() => {
    this.next()
  }, 1000)
})
const fn3 = new Chain(function () {
  console.log(3)
})
fn1.setNextSuccessor(fn2).setNextSuccessor(fn3)
fn1.passRequest()
*/
/*
const chainOrder500 = new Chain(order500)
const chainOrder200 = new Chain(order200)
const chainOrderNormal = new Chain(orderNormal)

chainOrder500.setNextSuccessor(chainOrder200)
chainOrder200.setNextSuccessor(chainOrderNormal)

// chainOrder500.setNextSuccessor(chainOrder200)
// chainOrder200.setNextSuccessor(chainOrderNormal)

chainOrder500.passRequest(2, true, 500)
*/

Function.prototype.after = function (fn) {
  const _this = this
  // console.log(this)
  return function() {
    const ret = _this.apply(this, arguments)
    if (ret === 'nextSuccessor') {
      return fn.apply(this, arguments)
    }
    return ret
  }
}
// ---------------------------
// order500 = function () {
//   const ret = _this.apply(this, arguments)
//   if (ret === 'nextSuccessor') {
//     return fn.apply(this, arguments)
//   }
//   return ret
// }.after(orderNormal) = function () {
//   const ret = _this.apply(this, arguments)
//   if (ret === 'nextSuccessor') {
//     return fn.apply(this, arguments)
//   }
//   return ret
// }
//----------------------------------
const order = order500
              .after(order200)
              .after(orderNormal)
              // console.log(new order())
// console.log(order500.after(order200))
// console.log(order)
order(1, true, 500)
