/**
 * 发布订阅又叫做观察者模式，它定义对象间的一对多的依赖关系，当一个对象的状态发生变化时，所有依赖于它的对象都会得到通知。
 * 发布-订阅模式可以广泛应用于异步编程中，这是一种替代传递回调函数的方案
 * 
 * 发布-订阅模式可以取代对象之间硬编码的通知机制，一个对象不用再显式的的调用另一个对象的某个接口。发布-订阅模式让两个对象松耦合的联系在一起，虽然不太清楚彼此的细节，但这不影响它们之间的通信。当有新的订阅者出现时，发布者的代码不需要任何修改，同样发布者需要改变时，也不会影响到之前的订阅者。只要它们之间的约定事件没有改变，就可以自由的设计它们。
 * 
 * 发布-订阅模式经常出现在我们的工作场景，如：当你给DOM绑定一个事件就使用了发布-订阅，我们向DOM上绑定了click事件，当DOM被点击的时候会向订阅者发布消息。
 */

/**
 * DOM事件
 */
/*
  document.body.addEventListener('click', function () {
    alert(2);
  }, false);
  document.body.addEventListener('click', function () {
    alert(3);
  }, false);
  document.body.addEventListener('click', function () {
    alert(4);
  }, false);
*/

/**
 * 自定义事件
 * 
 * 实现发布-订阅模式的思路
 *  首先指定好谁充当发布者
 *  然后给发布者添加一个缓存列表，用于存放回掉函数以便通知订阅者
 *  最后发布消息的时候，发布者会遍历这个缓存列表，以此触发里面存放的订阅者回掉函数
 * 另外还可以往回调函数中填入一些参数，订阅者可以接受这些参数。
 */
/*
const salesOffices = {} // 发布者

salesOffices.clientList = [] // 给发布者添加缓存列表，存放订阅者的回调函数，以便通知订阅者

salesOffices.listen = function(fn){// 添加订阅者
  this.clientList.push(fn) // 订阅的消息添加进缓存列表
}

salesOffices.trigger = function(){//发布消息
  for(let i = 0,fn;fn = this.clientList[i++];){
    fn.apply(this,arguments)
  }
}
// 订阅消息
salesOffices.listen((price,squareMeter)=>{
  console.log(`价格：${price},squareMeter${squareMeter}`)
})
salesOffices.listen((price,squareMeter)=>{
  console.log(`价格：${price},squareMeter${squareMeter}`)
})
// 发布消息
salesOffices.trigger(200000,50)
// salesOffices.trigger(500000,100)
*/

// 另一种写法让发布消息到指定的用户，而不是每个用户都发布
/*
const salesOffices = {} // 定义售楼处
// 给发布者添加缓存列表，存放订阅者的回调函数，以便通知订阅者
salesOffices.lientList = {}
// 添加订阅者  接受订阅
salesOffices.listen = function (key, fn) {
  if (!this.lientList[key]) {
    this.lientList[key] = []
  }
  this.lientList[key].push(fn)
}
//发布消息  发布事件
salesOffices.trigger = function () {
  const key = Array.prototype.shift.call(arguments)
  const fns = this.lientList[key]
  if (!fns || fns.length === 0) {
    return false
  }
  for (let i = 0, fn; fn = fns[i++];) {
    fn.apply(this, arguments)
  }
}

salesOffices.listen('xiaoming', (price, squareMeter) => {
  console.log(`价格：${price},squareMeter:${squareMeter}`)
})

salesOffices.listen('xiaoming', (price, squareMeter) => {
  console.log(`价格：${price},squareMeter:${squareMeter}`)
})

salesOffices.listen('xiaohong', (price, squareMeter) => {
  console.log(`价格：${price},squareMeter:${squareMeter}`)
})

salesOffices.trigger('xiaoming', 1000000, 91)
salesOffices.trigger('xiaohong', 1400000, 120)
*/
/** 发布订阅模式的通用实现
 * 需求是这样的，以上salesOffices拥有接受订阅和发布事件功能，可以怎么样让让其他对象也拥有类似的功能，如果在新对象里面重写一次明显是不明智的。下面我们通过将它们提取出来单独的放在一个对象里面
 * 
 * 
 */

const event = {
  lientList: {},
  listen: function (key, fn) {
    if (!this.lientList[key]) {
      this.lientList[key] = []
    }
    this.lientList[key].push(fn)
  },
  trigger: function () {
    const key = Array.prototype.shift.call(arguments)
    const fns = this.lientList[key]
    if (!fns || fns.length === 0) {
      return false
    }
    for (let i = 0, fn; fn = fns[i++];) {
      fn.apply(this, arguments)
    }
  },
  // 取消订阅的时候
  remove:function(key,fn){
    const fns = this.lientList[key]
    if(!fns){
      return false
    }
    if(!fn){
      fns.length = 0
    }else {
      for(let i = fns.length-1;i>=0;i--){
        if(fns[i] === fn){
          fns.splice(i,1)
        }
      }
    }
  }
}

function installEvent(obj,e){
  for (const key in e) {
    obj[key] = e[key]
  }
}

const salesOffices = {}
installEvent(salesOffices,event)
// 订阅消息
salesOffices.listen('xiaoming',(price,squareMeter)=>{
  console.log(`户主：xiaoming,平米：${squareMeter},价格：${price}`)
})
salesOffices.listen('lanlan',fn1 = (price,squareMeter)=>{
  console.log(`户主：lanlan,平米：${squareMeter},价格：${price}`)
})
salesOffices.listen('lanlan',fn2 = (price,squareMeter)=>{
  console.log(`户主：lanlan,平米：${squareMeter},价格：${price}`)
})
// salesOffices.remove('lanlan',fn2)
// salesOffices.trigger('lanlan',66,888888) 发布消息


// 
const login = {}
installEvent(login,event)

setTimeout(()=>{ // 模拟ajax请求
  const data = {
    code:200,msg:'成功'
  }
  login.trigger('loginSucc',data)//发布消息
},1000)

const header = (function(){
  login.listen('loginSucc',function(data){//订阅消息
    header.setAvatar(data.msg)
  })

  return {
    setAvatar:function(data){
      console.log('header:'+data)
    }
  }
})()

const nav = (function(){
  login.listen('loginSucc',function(data){//订阅消息
    nav.setAvatar(data.msg)
  })

  return {
    setAvatar:function(data){
      console.log('nav:'+data)
    }
  }
})()

// 全局发布-订阅对象  +  模块间通信
const global = {}
installEvent(global,event)

const btn = (function(){
  const _btn = document.getElementById('count')
  let count = 1
  _btn.onclick = function(){
    global.trigger('show',count++)
  }
})();

const show = (function(){
  const _show = document.getElementById('show')
  global.listen('show',(count)=>{
    _show.innerHTML = count
  })
})();

/**
 * 前面的知识点都是必须订阅者先订阅了一个消息，然后才能接受到发布者的消息，如果颠倒这种逻辑，如先发布消息，等待订阅消息再发送。
 * 满足这个需求，需要建立一个存放离线事件的堆栈，当事件发布的时候，此时如果还没有订阅事件，可以暂时把发布事件的动作存放以函数包裹的形式存放在堆栈中，等到有对象来订阅此事件，再将遍历堆栈并且依次执行这些包装函数，也就是重新发布里面的事件。
 */

/**
 * 发布订阅模式的优点：
 *  1.完成时间上的解耦
 *  2.完成对象之间的解耦
 * 它既可以用在异步编程中，也可以完成更松耦合的代码编写。
 * 它还可以帮助实现别的设计模式，如：中介者模式。
 * 同时从架构上来看，无论是mvc还是mvvm都少不了发布-订阅模式的参与。
 * 
 * 缺点：
 *  首先创建订阅者本身是消耗一定的时间和内存的，也许最后该订阅一直未发生，但始终存在内存中。
 *  另外，发布订阅模式虽然弱化对象之间的联系，过度使用会导致对象与对象之间的必要联系也将被深埋在背后，最终导致程序难以跟踪和理解。
 */