/**
 * 程序开发中，很多时候不希望某个类天生就非常庞大，一次性包含许多职责。那么我们就可以使用装饰者模式。装饰者模式可以动态的给某个对象添加一些额外的职责，而不会影响从这个类中派生的其他对象。
 * 装饰者模式能够在不改变对象自身的基础上，在程序运行期间给对象动态的添加职责。
 * 装饰者模式将一个对象嵌入另一个对象之中，实际上相当于这个对象被另一个对象包装起来，形成一条包装链。请求随着这条链以此传递到所有对象，每个对象都有处理这条请求的机会。
 */
// 传统面向对象装饰者模式例子
/*
function Plane(){}
Plane.prototype.fire = function(){
  console.log('发生子弹')
}

function MissileDecorator(plane){
  this.plane = plane
}
MissileDecorator.prototype.fire = function(){
  this.plane.fire()
  console.log('发射导弹')
}
function AtomDecorator(plane){
  this.plane = plane
}
AtomDecorator.prototype.fire = function(){
  this.plane.fire()
  console.log('发射核武器')
}

let plane = new Plane()
plane = new MissileDecorator(plane)
plane = new AtomDecorator(plane)

plane.fire()
*/


// javascript的装饰者模式
/*
const plane = {
  fire:function(){
    console.log('发射普通子弹')
  }
}
function MissileDecorator(){
  console.log('发射导弹')
}
function AtomDecorator(){
  console.log('发射原子弹')
}

fire1 = plane.fire
plane.fire = function(){
  fire1()
  MissileDecorator()
}

fire2 = plane.fire
plane.fire = function(){
  fire2()
  AtomDecorator()
}

plane.fire()
*/
//装饰函数
// 通过保存引用的情况下解决在不碰原函数的情况下给方法新增其他特性。
/*
var a = function(){
  console.log(1)
}
let _a = a
var a = function(){
  _a()
  console.log(2)
}
a()
*/

/**APO的应用实例 
 * APO装饰的函数可以在业务代码或者框架层面，都可以把行为依照职责分成粒度更细的函数，随后通过装饰把他们合并在一起，以此编写一个松耦合和高复用性的系统。
*/
/*
Function.prototype.after = function(afterfn){
  const _this = this
  return function(){
    const ret = _this.apply(this,arguments)
    afterfn.apply(this,arguments)
    return ret
  }
}

let showLogin = function(){
  console.log('打开登录浮层')
}

let log = function(){
  console.log('上报标签为：'+this.getAttribute('tag'))
}

showLogin = showLogin.after(log)
document.getElementById('button').onclick = showLogin
*/
/*
Function.prototype.before = function(beforefn){
  const _this = this
  return function(){
    beforefn.apply(this,arguments)
    return _this.apply(this,arguments)
  }
}

let func = function(params){
  console.log(params)
}
func = func.before(function(params){
  params.b = 'b'
})
func({a:'a'})

let ajax = function(type,url,params){
  console.log(params)
}
function getToken(){
  return '***'
}
ajax = ajax.before(function(type,url,params){
  params.token = getToken()
})
ajax('post','xxx.com',{name:'pino'})
*/
const from = {
  username:'pino',
  password:333
}
Function.prototype.before = function(beforefn){
  const _this = this
  return function(){
    console.log(beforefn.apply(this,arguments))
    if(beforefn.apply(this,arguments) === false){
      return
    }
    _this.apply(this,arguments)
  }
}
function validata(){
  if(from.username === ''){
    return false
  }else if(from.password === ''){
    return false
  }
  return true
}


let formSubmit = function(params){
  const obj = {
    username:from.username,
    password:from.password
  }
  console.log(obj)
}
formSubmit = formSubmit.before(validata)
formSubmit()


