// 迭代器模式是指提供一种方法顺序访问一个聚合对象中的各个元素，并且不需要暴露该对象的内部表示。
// 迭代器模式把迭代的过程从业务逻辑中分离出来。在使用迭代器模式之后，即使不关心对象的内部构造，也可以按顺序去访问其中的每个元素



/** 内部迭代器
 * 内部迭代器及迭代器内部实现的迭代规则，外界无用关系迭代器的内部实现
 * 缺点：迭代规则已经约定好，如果有特性需求，无法满足，因此不够灵活，
 */
 Array.prototype.myEach = function(fn){
  for (let i = 0; i < this.length; i++) {
    fn.call(this[i],this[i],i,this)
  }
}

let arr = [1,2,3]
arr.myEach((item,i)=>{
  console.log(item,i)
})

/** 外部迭代器
 * 外部迭代器必须显式的请求迭代下一个元素
 * 外部迭代器增加了一些调用的复杂度，但相对也增强了迭代器的灵活性，可以手工控制迭代的过程或者顺序。
 * 外部迭代器就是将迭代相关的属性都暴露出来，后续处理交出
 */

function Iterator(obj){
  let current = 0
  const next = function(){
    current+=1
  }
  const isDone = function(){
    return current >= obj.length
  }

  const getCurrItem = function(){
    return obj[current]
  }

  return {
    next:next,
    isDone:isDone,
    getCurrItem,
    length:obj.length
  }
}

function compare(iterator1,iterator2){
  if(iterator1.length !== iterator2.length){
    throw new Error('不相等')
  }
  console.log(!iterator1.isDone(),!iterator2.isDone())
  while(!iterator1.isDone()&&!iterator2.isDone()){
    console.log(!iterator1.isDone(),!iterator2.isDone())
    if(iterator1.getCurrItem() !== iterator2.getCurrItem()){
      throw new Error('iterator1 和 iterator2 不相等')
    }
    iterator1.next()
    iterator2.next()
  }
  console.log('相等')
}
compare(Iterator([1,2,3]),Iterator([1,2,3]))