// 代理模式的关键是：当客户不方便直接访问一个对象或者不满足需要的时候，提供一个替身对象来控制对这个对象的访问，客户实际上访问的是替身对象。替身对象对请求做出一些处理之后，再把请求转交给本体。

/*
function Flower(){}
const xiaoming = {
  sendFlower:function(target){
    const flower = new Flower()
    target.receiveFlower(flower)
  }
}
const A = {
  receiveFlower:function(flower){
    console.log('收到花')
  }
}

xiaoming.sendFlower(A)
*/
// 代理模式
/*
function Flower(){}

const xiaoming = {
  sendFlower:function(target){
    // const flower = new Flower()
    target.receiveFlower()
  }
}
// 代理对象
// 代理对象除了可以替本体对象做黑脸的身份，还可以把一些开销较大的操作交给它做，如new XXX，这样可以保存只在需要的时候才去做，减少不必要的开销
const B = {
  receiveFlower:function(){
    A.listenGoodMood(()=>{ // 监听A的好心情再去送花
      // const flower = new Flower()
      A.receiveFlower(flower)
    })
  }
}

const A = {
  receiveFlower:function(flower){
    console.log('收到花')
  },
  listenGoodMood:function(fn){
    setTimeout(function(){
      fn()
    },3000)
  }
}

xiaoming.sendFlower(B)

*/

/**
 * 虚拟代理实现图片预加载
 */

/*
const myImg = (function(){
  const imgNode = document.createElement('img')
  document.body.append(imgNode)

  return {
    setSrc:function(src){
      imgNode.src = src
    }
  }
})()

const proxyImg = (function(){
  const img = new Image
  img.onload = function(){
    myImg.setSrc(this.src)
  }
  return {
    setSrc:function(src){
      myImg.setSrc('https://img2.baidu.com/it/u=2653368542,1746961883&fm=253&fmt=auto&app=138&f=JPEG?w=760&h=500')
      img.src=src
    }
  }
})()
*/

// proxyImg.setSrc('https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.jj20.com%2Fup%2Fallimg%2F1111%2F100QQ04518%2F1Q00Q04518-1.jpg&refer=http%3A%2F%2Fpic.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1647499957&t=95b525b6e122bec31e02c84459d2c9ed')

/**
 * 代理的意义是符合开放-封闭原则的，在发生变故的情况下，只需要在代理中去修改，不会影响到主体，代理和主体各自不会影响到对方。否则所有业务逻辑都是在主体中实现，在发生变化时，及强耦合性会影响到另外一个值则的实现。
 */

/**
 * 代理和本体接口的一致性，代理与本体之间可以说是鸭子类型的关系，不在乎他怎么实现的，只要它们之间暴露的接口以及方法是一致的即可。
 */

/**
 * 虚拟代理合并HTTP请求
 * 
 */
function synchronousFile(id) {
  console.log('开始同步文件，id为：' + id)
}
// 通过代理模式将频繁点击check收集起来，分段再一次性发送出去。
const proxySynchronousFile = (function () {
  const cache = []
  let timer = null

  return function (id) {
    cache.push(id)
    if (timer) {
      return
    }
    timer = setInterval(() => {
      synchronousFile(cache.join(','))
      cache.length = 0
      clearInterval(timer)
      timer = null
    }, 1200)
  }
})()

const checkBox = document.getElementsByTagName('input')
// 给每个check绑定事件
for (let i = 0, check; check = checkBox[i++];) {
  check.onclick = function () {
    if (this.checked) {
      proxySynchronousFile(this.id)
    }
  }
}

/**
 * 虚拟代理在惰性加载中的应用。
 */
/*
let miniConsole = (function(){
  const cache = []
  const handler = function(ev){
    if(ev.keyCode === 113){
      const script = document.createElement('script')
      script.onload = function(){
        for(var i = 0,fn;fn = cache[i++];){
          fn()
        }
      }
      script.src = './设计模式/miniConsole.js'
      document.getElementsByTagName('head')[0].append(script)
      document.body.removeEventListener('keydown',handler)// 只加载一次miniConsole.js
    }
  }

  document.body.addEventListener('keydown',handler,false)

  return {
    log:function(){
      const args = arguments
      cache.push(function(){
        return miniConsole.log.apply(miniConsole,args)
      })
    }
  }
})()

miniConsole.log(1)
*/

/**
 * 缓存代理
 * 为一些开销大的运行结果提供暂时的存储，如果在下次运算时，传递进来的参数跟之前的一致，则从缓存中直接返回运算结果
 */
// 例子1 计算乘积
function mult() {
  console.log('开始乘积')
  let a = 1
  for (let i = 0; i < arguments.length; i++) {
    a = a * arguments[i]
  }
  return a
}
function plus() {
  let a = 1
  for (let i = 0; i < arguments.length; i++) {
    a = a + arguments[i]
  }
  return a
}
// 代理对象
// 将参数拼接作为key，存在缓存中，下次调用遇到相同参数直接从缓存中返回结果
const proxyMult = (function () {
  const cache = {}
  return function () {
    const args = Array.prototype.join.call(arguments, ',');
    if (args in cache) {
      return cache[args]
    }
    return cache[args] = mult.apply(this, arguments)
  }
})();

// 用高阶函数动态创建代理
const createProxyFactory = function (fn) {
  const cache = {}
  return function () {
    const args = Array.prototype.join.call(arguments, ',');
    if (args in cache) {
      return cache[args]
    }
    return cache[args] = fn.apply(this, arguments)
  }
};
let pMult = createProxyFactory(mult)
let pPlus = createProxyFactory(plus)
console.log(pMult(1, 2, 3, 4),pMult(1, 2, 3, 4))
console.log(pPlus(1, 2, 3, 4),pPlus(1, 2, 3, 4)) // 该参数与之前的一致，结果直接从缓存中读取。而不是重新去计算。

// 缓存代理用于ajax异步请求数据，如在参数页数和长度相同的情况下，几乎返回的数据相同。

/** 其他代理模式
 * 除了上述代理的几个模式，还有以下代理类型
 *    1.防护墙代理：控制网络资源的访问，保护主体不让坏人接近
 *    2.远程代理：为一个对象在不同的地址空间提供局部代表，在java中，远程代理可以是另一个虚拟机中的对象
 *    3.保护代理：用于对象应该有不同的访问权限的情况
 *    4.智能引用代理：取代了简单的指针，它在访问对象时执行一些附加操作，比如计算一个对象被引用的次数
 *    5.写时复制代理：通常用于复制一个庞大的对象。
 * 
 */
