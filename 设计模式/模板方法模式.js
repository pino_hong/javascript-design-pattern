// 模块方法模式是一种基于继承的设计模式，在javascript开发中用到的继承的场景并不是很多，但这并不代表继承在javascript里没有用武之地，虽然js没有真正意义上的类的继承，但是通过原型prototype可以变相的实现继承。


// 例子  咖啡与茶
/*
function Beverage(){
  this.name = 'pino'
}

Beverage.prototype.boilWater = function(){
  console.log('把水煮沸')
}
Beverage.prototype.brew = function(){
  throw new Error('必须重写brew方法')
}
Beverage.prototype.pourInCup = function(){}
Beverage.prototype.addCondiments = function(){
  throw new Error('必须重写addCondiments方法')
}
// 钩子方法 控制addCondiments是否是必须的
Beverage.prototype.customerWantsCondiments = function(){
  return true
}
Beverage.prototype.init = function(){
  this.boilWater()
  this.brew()
  this.pourInCup()
  if(this.customerWantsCondiments){
    this.addCondiments()
  }
}

function Coffee(){}
Coffee.prototype = new Beverage()
Coffee.prototype.brew = function(){
  console.log('用沸水冲泡咖啡')
}
Coffee.prototype.pourInCup = function(){
  console.log('把咖啡倒进杯子')
}
Coffee.prototype.addCondiments = function(){
  console.log('加糖和牛奶')
}
var coffee = new Coffee()
coffee.init()
console.log(coffee)
// -----------------------------------
function Tea(){}
Tea.prototype = new Beverage()
Tea.prototype.brew = function(){
  console.log('用沸水侵泡茶叶')
}
Tea.prototype.pourInCup = function(){
  console.log('把茶倒进杯子')
}
Tea.prototype.addCondiments = function(){
  console.log('加柠檬')
}
Tea.prototype.addCondiments = function(){
  return false
}
var tea = new Tea()
tea.init()
*/
/**
 * 具体类可以被实例
 * 抽象类不能直接被实例
 *  抽象类主要用来约束子类的一些行为，比如子类中必须存在抽象类的某个相同的方法。
 */

// 在不需要继承的方式实现模块方法模式
// 在javascript中，很多时候都不需要葫芦画瓢的去实现一个模板方法模式，高阶函数是更好的选择。
function Beverage(params){
  const boilWater = function(){
    console.log('把水煮沸')
  }
  const brew = params.brew || function(){
    throw new Error('字类必须存在 brew 方法')
  }
  const pourInCup = params.pourInCup || function(){
    throw new Error('字类必须存在 pourInCup 方法')
  }
  const addCondiments = params.addCondiments || function(){
    throw new Error('字类必须存在 addCondiments 方法')
  }

  function Foo(){}
  Foo.prototype.init = function(){
    boilWater()
    brew()
    pourInCup()
    addCondiments()
  }

  return Foo
}

const Coffee = Beverage({
  brew:function(){
    console.log('用沸水冲泡咖啡')
  },
  pourInCup:function(){
    console.log('把咖啡倒进杯子')
  },
  addCondiments:function(){
    console.log('加糖和牛奶')
  },
})

const coffee = new Coffee()
coffee.init()

const Tea = Beverage({
  brew:function(){
    console.log('用沸水侵泡茶叶')
  },
  pourInCup:function(){
    console.log('把茶倒进杯子')
  },
  addCondiments:function(){
    console.log('加柠檬')
  },
})

const tea = new Tea()
tea.init()
